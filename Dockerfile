# using python3.5 as raspbian has no newer

FROM python:3.5-stretch

COPY requirements.txt /root/
RUN pip install -r /root/requirements.txt
