#!/usr/bin/env python3
from nordpool import elspot
from datetime import date, datetime, timezone
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(17, GPIO.OUT)

MIN_HEATING_HOURS = 5
ALWAYS_HEAT_PRICE = 10
start_time = datetime.now(timezone.utc)
print(
    str(datetime.now(timezone.utc)),
    " Starting nordpool run, HEATING_HOURS are",
    MIN_HEATING_HOURS,
    "cheapest",
)

prices_spot = elspot.Prices()


def fetch_prices(area="EE", prices_date=date.today()):
    raw_data = prices_spot.hourly(areas=[area], end_date=prices_date)["areas"][area][
        "values"
    ]
    print(str(datetime.now(timezone.utc)), "raw hourly data", raw_data)
    return raw_data


def heating_hours(
    price_data, min_heating_hours, always_heat_price, curnow=datetime.now(timezone.utc)
):
    sorted_data = sorted(price_data, key=lambda k: k["value"])
    for i, entry in enumerate(sorted_data):
        if entry["start"] < curnow < entry["end"]:
            if entry["value"] < always_heat_price:
                print(
                    str(datetime.now(timezone.utc)),
                    "ON: current hour price",
                    entry["value"],
                    "is less than ALWAYS_HEAT_PRICE",
                    always_heat_price,
                )
                boiler_control(True)
                return True
            elif i < min_heating_hours:
                print(
                    str(datetime.now(timezone.utc)),
                    "ON: heating during the cheapest",
                    min_heating_hours,
                    "hours. Price:",
                    entry["value"],
                )
                boiler_control(True)
                return True
            else:
                print(
                    str(datetime.now(timezone.utc)),
                    "OFF: current price:",
                    entry["value"],
                )
                boiler_control(False)
                return True


def boiler_control(state):
    GPIO.output(17, state)


heating_hours(fetch_prices(), MIN_HEATING_HOURS, ALWAYS_HEAT_PRICE)

print(
    datetime.now(timezone.utc),
    "Exiting: duration:",
    str(datetime.now(timezone.utc) - start_time),
)
